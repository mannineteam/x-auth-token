<?php namespace Mannine\XAuthToken;

use Illuminate\Support\ServiceProvider;
use Mannine\XAuthToken\Filters\XAuthTokenFilter;

/**
 * Class XAuthTokenServiceProvider
 * @package Mannine\XAuthToken
 */
class XAuthTokenServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;


    public function boot(){
        $this->package('mannine/x-auth-token');
        $this->app['router']->filter('xauth.token', 'mannine.xauth.token.filter');
    }

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register(){

        $app = $this->app;

        $app->bindShared('mannine.xauth.token', function ($app) {
            return new Managers\XAuthTokenManager($app);
        });

        $app->bindShared('mannine.xauth.token.filter', function ($app) {
            $driver = $app['mannine.xauth.token']->driver();
            $events = $app['events'];

            return new XAuthTokenFilter($driver, $events);
        });

        $app->bind('Mannine\XAuthToken\XAuthTokenController', function ($app) {
            $driver         = $app['mannine.xauth.token']->driver();
            $credsFormatter = $app['config']->get('x-auth-token::format_credentials', null);
            return new XAuthTokenController($driver, $credsFormatter);
        });

	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides(){
        return array('mannine.xauth.token', 'mannine.xauth.token.filter');
	}

}