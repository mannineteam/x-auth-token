<?php namespace Mannine\XAuthToken\Filters;

use Illuminate\Events\Dispatcher;
use Mannine\XAuthToken\Exceptions\NotAuthorizedException;
use Mannine\XAuthToken\Drivers\XAuthTokenDriver;

/**
 * Class XAuthTokenFilter
 * @package Mannine\XAuthToken\Filters
 */
class XAuthTokenFilter {

    /**
    * The event dispatcher instance.
    *
    * @var \Illuminate\Events\Dispatcher
    */
    protected $events;

    /**
     * @var \Mannine\XAuthToken\Drivers\XAuthTokenDriver
     */
    protected $driver;

    /**
     * __construct
     *
     * @param XAuthTokenDriver $driver
     * @param Dispatcher $events
     */
    function __construct(XAuthTokenDriver $driver, Dispatcher $events){
        $this->driver = $driver;
        $this->events = $events;
    }

    /**
     * Check auth filter
     * @param $route
     * @param $request
     * @throws \Mannine\XAuthToken\Exceptions\NotAuthorizedException
     */
    function filter($route, $request) {
        $payload = $request->header('X-Auth-Token');

        if(empty($payload))
            $payload = $request->input('auth_token');


        $user = $this->driver->validate($payload);

        if(!$user)
            throw new NotAuthorizedException();

        $this->events->fire('xauth.token.valid', $user);
    }
}