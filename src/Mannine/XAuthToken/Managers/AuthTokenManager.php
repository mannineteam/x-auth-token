<?php namespace Mannine\XAuthToken\Managers;

use Illuminate\Support\Manager;
use Mannine\XAuthToken\Drivers\XAuthTokenDriver;
use Mannine\XAuthToken\Providers\HashProvider;
use Mannine\XAuthToken\Providers\DatabaseAuthTokenProvider;


/**
 * Class XAuthTokenManager
 * @package Mannine\XAuthToken\Managers
 */
class XAuthTokenManager extends Manager {

    protected function createDatabaseDriver() {
        $provider   = $this->createDatabaseProvider();
        $users      = $this->app['auth']->driver()->getProvider();

        return new XAuthTokenDriver($provider, $users);
    }

    /**
     * @return DatabaseAuthTokenProvider
     */
    protected function createDatabaseProvider() {
        $connection = $this->app['db']->connection();
        $encrypter  = $this->app['encrypter'];
        $hasher     = new HashProvider($this->app['config']['app.key']);

        return new DatabaseAuthTokenProvider($connection, 'mn_auth_tokens', $encrypter, $hasher);
    }

    /**
     * @return string
     */
    protected function getDefaultDriver() {
        return 'database';
    }
}