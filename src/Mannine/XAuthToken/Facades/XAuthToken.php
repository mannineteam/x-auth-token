<?php namespace Mannine\XAuthToken\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * Class XAuthToken
 * @package Mannine\XAuthToken\Facades
 */
class XAuthToken extends Facade {
    protected static function getFacadeAccessor() { return 'mannine.xauth.token'; }
}