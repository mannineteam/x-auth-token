<?php namespace Mannine\XAuthToken\Drivers;

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\UserProviderInterface;

use Mannine\XAuthToken\XAuthToken;
use Mannine\XAuthToken\Providers\XAuthTokenProviderInterface;

use Mannine\XAuthToken\Exceptions\NotAuthorizedException;

class XAuthTokenDriver {

    /**
     * @var \Mannine\XAuthToken\Providers\XAuthTokenProviderInterface
     */
    protected $tokens;

    /**
    * @var \Illuminate\Auth\UserProviderInterface
    */
    protected $users;

    /**
     * @param XAuthTokenProviderInterface $tokens
     * @param UserProviderInterface $users
     */
    function __construct(XAuthTokenProviderInterface $tokens, UserProviderInterface $users){
        $this->tokens = $tokens;
        $this->users = $users;
    }

    /**
     * Returns the AuthTokenInterface provider.
     */
    public function getProvider(){
        return $this->tokens;
    }

    /**
     * Validates a public auth token. Returns User object on success, otherwise false.
     *
     * @param $authTokenPayload
     * @return bool|UserInterface|null
     */
    public function validate($authTokenPayload) {

        if($authTokenPayload == null)   return false;

        $tokenResponse = $this->tokens->find($authTokenPayload);

        if($tokenResponse == null)      return false;

        $user = $this->users->retrieveByID($tokenResponse->getAuthIdentifier());

        if($user == null)               return false;

        return $user;
    }

    /**
     * Attempt to create an AuthToken from user credentials.
     *
     * @param array $credentials
     * @return bool
     */
    public function attempt(array $credentials) {
        $user = $this
            ->users
            ->retrieveByCredentials($credentials);

        if($user instanceof UserInterface && $this->users->validateCredentials($user, $credentials)) {
            return $this->create($user, false, false);
        }

        return false;
    }

    /**
     * Create auth token for user.
     *
     * @param UserInterface $user
     * @param $type
     * @param $deviceId
     * @return mixed
     */
    public function create(UserInterface $user, $type, $deviceId) {
        $this->tokens->purge($user, $type, $deviceId);
        return $this->tokens->create($user, $type, $deviceId);
    }


    /**
    * Retrive user from auth token.
    *
    * @param XAuthToken $token
    * @return UserInterface|null
    */
    public function user(XAuthToken $token) {
        return $this->users->retrieveByID( $token->getAuthIdentifier() );
    }

    /**
     * Serialize token for public use.
     *
     * @param XAuthToken $token
     * @return string
     */
    public function publicToken(XAuthToken $token) {
        return $this->tokens->serializeToken($token);
    }
}