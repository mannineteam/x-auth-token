<?php namespace Mannine\XAuthToken;

use Illuminate\Routing\Controller;
use Mannine\XAuthToken\Exceptions\NotAuthorizedException;
use Mannine\XAuthToken\Drivers\XAuthTokenDriver;

/**
 * Class XAuthTokenController
 * @package Mannine\XAuthToken
 */
class XAuthTokenController extends Controller {


    protected $driver;

    /**
    * @var callable format username and password into hash for Auth::attempt
    */
    protected $credentialsFormatter;

    /**
     * @param AuthTokenDriver $driver
     * @param callable $credentialsFormatter
     */
    function __construct(XAuthTokenDriver $driver, \Closure $credentialsFormatter){
        $this->driver               = $driver;
        $this->credentialsFormatter = $credentialsFormatter;
    }

    /**
     * @return mixed
     */
    protected function getAuthToken() {

        $token = \Request::header('X-Auth-Token');

        if(empty($token))
            $token = \Input::get('auth_token');

        return $token;
    }

    /**
     * @return mixed
     * @throws Exceptions\NotAuthorizedException
     */
    public function index() {

        $payload    = $this->getAuthToken();
        $user       = $this->driver->validate($payload);

        if(!$user)
            throw new NotAuthorizedException();

        return \Response::json($user);
    }

    /**
     * @return mixed
     * @throws Exceptions\NotAuthorizedException
     */
    public function store() {

        $input = \Input::all();

        $validator = \Validator::make(
            $input, array(
                'username'  => array('required'),
                'password'  => array('required'),
                'device_id' => array('required'),
            )
        );

        if($validator->fails())
            throw new NotAuthorizedException();

        $creds = call_user_func($this->credentialsFormatter, $input['username'], $input['password']);
        $token = $this->driver->attempt($creds);

        if(!$token)
            throw new NotAuthorizedException();

        $serializedToken    = $this->driver->getProvider()->serializeToken($token);
        $user               = $this->driver->user($token);

        return \Response::json(array('token' => $serializedToken, 'user' => $user->toArray()));
    }

    /**
     * @return mixed
     * @throws Exceptions\NotAuthorizedException
     */
    public function destroy() {
        $payload    = $this->getAuthToken();
        $user       = $this->driver->validate($payload);

        if(!$user)
            throw new NotAuthorizedException();

        $this->driver->getProvider()->purge($user->id);

        return \Helpers\Rest::response(200, array());
    }
}