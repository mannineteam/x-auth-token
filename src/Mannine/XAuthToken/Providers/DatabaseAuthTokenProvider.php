<?php namespace Mannine\XAuthToken\Providers;

use \Illuminate\Auth\UserInterface;
use \Illuminate\Database\Connection;
use Illuminate\Encryption\Encrypter;
use Mannine\XAuthToken\Exceptions\NotAuthorizedException;

/**
 * Class DatabaseAuthTokenProvider
 * @package Mannine\XAuthToken\Providers
 */
class DatabaseAuthTokenProvider extends AbstractAuthTokenProvider {

    /**
    * @var \Illuminate\Database\Connection
    */
    protected $conn;

    /**
     * @var HashProvider
     */
    protected $table;

    /**
     * @param Connection $conn
     * @param HashProvider $table
     * @param Encrypter $encrypter
     * @param HashProvider $hasher
     */
    public function __construct(Connection $conn, $table, Encrypter $encrypter, HashProvider $hasher){
        parent::__construct($encrypter, $hasher);

        $this->table    = $table;
        $this->conn     = $conn;
    }

    /**
     * @return Connection
     */
    public function getConnection(){
        return $this->conn;
    }

    /**
    * @return \Illuminate\Database\Query\Builder
    */
    protected function db() {
        return $this->conn->table($this->table);
    }

    /**
     * Creates an auth token for user.
     *
     * @param UserInterface $user
     * @param string $type
     * @param $deviceId
     * @return bool|\Mannine\XAuthToken\XAuthToken|mixed
     */
    public function create(UserInterface $user, $type, $deviceId){

        if($user == null || $user->getAuthIdentifier() == null)
            return false;

        $token = $this->generateAuthToken();
        $token->setAuthIdentifier($user->getAuthIdentifier());
        $token->setDeviceID($deviceId);
        $token->setType($type);
        $token->setToken($this->serializeToken($token));

        $t     = date('Y-m-d H:i:s',time());
        $insertData = array_merge($token->toArray(), array(
            'created_at' => $t,
            'updated_at' => $t
        ));

        $this->db()->insert($insertData);

        return $token;
    }

    /**
     * Find user id from auth token.
     *
     * @param $serializedAuthToken
     * @return \Mannine\XAuthToken\XAuthToken|mixed|null
     */
    public function find($serializedAuthToken){

        $authToken = $this->deserializeToken($serializedAuthToken);

        if($authToken == null)                  return null;
        if(!$this->verifyAuthToken($authToken)) return null;

        $res = $this->db()
            ->where('auth_identifier',  $authToken->getAuthIdentifier())
            ->where('public_key',       $authToken->getPublicKey())
            ->where('private_key',      $authToken->getPrivateKey())
            ->first();

        if($res == null)
            return null;

        return $authToken;
    }


    /**
     * Update current row for user|type|deviceId
     *
     * @param $identifier
     * @param $type
     * @param $deviceId
     * @return bool|mixed
     */
    public function purge($identifier, $type = false, $deviceId = false){


        if($identifier instanceof UserInterface)
            $identifier = $identifier->getAuthIdentifier();

        if(!$type && !$deviceId){

            $token = \Request::header('X-Auth-Token');
            
            if(empty($token))
                $token = \Input::get('auth_token');


            $res = $this->db()
                    ->where('auth_identifier',  $identifier)
                    ->where('token',            $token)
                ->delete();

        } else
            $res = $this->db()
                ->where('auth_identifier',  $identifier)
                ->where('type',             $type)
                ->where('device_id',        $deviceId)
            ->delete();

        return $res > 0;
    }

}