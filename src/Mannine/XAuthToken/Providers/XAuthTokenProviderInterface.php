<?php namespace Mannine\XAuthToken\Providers;

use Illuminate\Auth\UserInterface;
use Mannine\XAuthToken\XAuthToken;

/**
 * Interface XAuthTokenProviderInterface
 * @package Mannine\XAuthToken\Providers
 */
interface XAuthTokenProviderInterface {

    /**
     * Creates an auth token for user.
     *
     * @param UserInterface $user
     * @param string $type
     * @param $deviceId
     * @return mixed
     */
    public function create(UserInterface $user, $type, $deviceId);

    /**
     * Find user id from auth token.
     *
     * @param $serializedAuthToken
     * @return mixed
     */
    public function find($serializedAuthToken);

    /**
     * Returns serialized token.
     *
     * @param XAuthToken $token
     * @return mixed
     */
    public function serializeToken(XAuthToken $token);

    /**
     * Deserializes token.
     *
     * @param $payload
     * @return mixed
     */
    public function deserializeToken($payload);

    /**
     * Purge token
     *
     * @param $identifier
     * @param $type
     * @param $deviceId
     * @return mixed
     */
    public function purge($identifier, $type, $deviceId);
}